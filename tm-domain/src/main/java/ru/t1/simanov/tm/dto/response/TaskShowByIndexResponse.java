package ru.t1.simanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.model.Task;

@NoArgsConstructor
public final class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
