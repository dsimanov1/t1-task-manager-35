package ru.t1.simanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonLoadFasterXmlRequest extends AbstractUserRequest {

    public DataJsonLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
