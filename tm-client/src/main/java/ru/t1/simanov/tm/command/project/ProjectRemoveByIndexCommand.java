package ru.t1.simanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.ProjectRemoveByIndexRequest;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove project by index.";

    @NotNull
    public static final String NAME = "project-remove-by-index";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().removeByIndexProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
