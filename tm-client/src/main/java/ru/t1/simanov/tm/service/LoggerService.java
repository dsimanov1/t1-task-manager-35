package ru.t1.simanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "./commands.xml";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = "./errors.xml";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "./messages.xml";

    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    @NotNull
    private static final Logger LOGGER_COMMAND = getLoggerCommand();

    @NotNull
    private static final Logger LOGGER_ERROR = getLoggerError();

    @NotNull
    private static final Logger LOGGER_MESSAGE = getLoggerMessage();

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(LOGGER_COMMAND, COMMANDS_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGES_FILE, true);
    }

    @NotNull
    public static Logger getLoggerCommand() {
        return Logger.getLogger(COMMANDS);
    }

    @NotNull
    public static Logger getLoggerError() {
        return Logger.getLogger(ERRORS);
    }

    @NotNull
    public static Logger getLoggerMessage() {
        return Logger.getLogger(MESSAGES);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

}
