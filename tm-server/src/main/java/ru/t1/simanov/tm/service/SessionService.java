package ru.t1.simanov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.repository.ISessionRepository;
import ru.t1.simanov.tm.api.service.ISessionService;
import ru.t1.simanov.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}
