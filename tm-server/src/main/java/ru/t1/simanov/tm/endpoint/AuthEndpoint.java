package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.simanov.tm.api.service.IAuthService;
import ru.t1.simanov.tm.api.service.IServiceLocator;
import ru.t1.simanov.tm.api.service.IUserService;
import ru.t1.simanov.tm.dto.request.UserLoginRequest;
import ru.t1.simanov.tm.dto.request.UserLogoutRequest;
import ru.t1.simanov.tm.dto.request.UserViewProfileRequest;
import ru.t1.simanov.tm.dto.response.UserLoginResponse;
import ru.t1.simanov.tm.dto.response.UserLogoutResponse;
import ru.t1.simanov.tm.dto.response.UserViewProfileResponse;
import ru.t1.simanov.tm.model.Session;
import ru.t1.simanov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.simanov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return this.getServiceLocator().getAuthService();
    }

    @NotNull
    private IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse loginUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @NotNull final String token = getAuthService().login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logoutUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull final Session session = check(request);
        getAuthService().logout(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse viewProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = getUserService().findOneById(userId);
        return new UserViewProfileResponse(user);
    }

}
