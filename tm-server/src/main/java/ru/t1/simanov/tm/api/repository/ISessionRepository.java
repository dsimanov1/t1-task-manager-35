package ru.t1.simanov.tm.api.repository;

import ru.t1.simanov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
