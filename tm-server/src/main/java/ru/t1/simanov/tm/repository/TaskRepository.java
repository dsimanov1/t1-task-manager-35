package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.repository.ITaskRepository;
import ru.t1.simanov.tm.model.Task;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return Objects.requireNonNull(add(task));
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return Objects.requireNonNull(add(task));
    }

    @NotNull
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return models
                .stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
