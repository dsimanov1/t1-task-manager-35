package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.api.service.IServiceLocator;
import ru.t1.simanov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.simanov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.simanov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.simanov.tm.dto.response.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.simanov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getApplicationAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getApplicationVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
