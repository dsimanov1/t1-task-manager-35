package ru.t1.simanov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.comparator.NameComparator;
import ru.t1.simanov.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static Project USER_PROJECT1 = new Project();

    @NotNull
    public final static Project USER_PROJECT2 = new Project();

    @NotNull
    public final static Project ADMIN_PROJECT1 = new Project();

    @NotNull
    public final static Project ADMIN_PROJECT2 = new Project();

    @Nullable
    public final static Project NULL_PROJECT = null;

    @NotNull
    public final static String NON_EXISTING_PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<Project> USER_PROJECT_LIST = Arrays.asList(USER_PROJECT1, USER_PROJECT2);

    @NotNull
    public final static List<Project> ADMIN_PROJECT_LIST = Arrays.asList(ADMIN_PROJECT1, ADMIN_PROJECT2);

    @NotNull
    public final static List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    public final static List<Project> SORTED_PROJECT_LIST = new ArrayList<>();

    static {
        USER_PROJECT_LIST.forEach(project -> project.setUserId(UserTestData.USER_TEST.getId()));
        USER_PROJECT_LIST.forEach(project -> project.setName("User Test Project " + project.getId()));
        USER_PROJECT_LIST.forEach(project -> project.setDescription("User Test Project " + project.getId() + " description"));
        ADMIN_PROJECT_LIST.forEach(project -> project.setUserId(UserTestData.ADMIN_TEST.getId()));
        ADMIN_PROJECT_LIST.forEach(project -> project.setName("Admin Test Project " + project.getId()));
        ADMIN_PROJECT_LIST.forEach(project -> project.setDescription("Admin Test Project " + project.getId() + " description"));
        PROJECT_LIST.addAll(USER_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN_PROJECT_LIST);
        SORTED_PROJECT_LIST.addAll(PROJECT_LIST);
        SORTED_PROJECT_LIST.sort(NameComparator.INSTANCE);
    }

}
