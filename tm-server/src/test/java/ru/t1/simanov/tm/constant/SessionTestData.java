package ru.t1.simanov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.model.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static Session USER_SESSION1 = new Session();

    @NotNull
    public final static Session USER_SESSION2 = new Session();

    @NotNull
    public final static Session ADMIN_SESSION1 = new Session();

    @NotNull
    public final static Session ADMIN_SESSION2 = new Session();

    @Nullable
    public final static Session NULL_SESSION = null;

    @NotNull
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<Session> USER_SESSION_LIST = Arrays.asList(USER_SESSION1, USER_SESSION2);

    @NotNull
    public final static List<Session> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1, ADMIN_SESSION2);

    @NotNull
    public final static List<Session> SESSION_LIST = new ArrayList<>();

    static {
        USER_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USER_TEST.getId()));
        ADMIN_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.ADMIN_TEST.getId()));
        SESSION_LIST.addAll(USER_SESSION_LIST);
        SESSION_LIST.addAll(ADMIN_SESSION_LIST);
    }

}
