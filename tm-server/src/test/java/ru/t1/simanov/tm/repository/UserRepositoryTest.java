package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.marker.UnitCategory;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.service.PropertyService;
import ru.t1.simanov.tm.util.HashUtil;

import static ru.t1.simanov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Before
    public void before() {
        repository.add(USER_TEST);
    }

    @After
    public void after() {
        repository.removeAll(USER_LIST);
    }

    @Test
    public void add() {
        Assert.assertNull(repository.add(NULL_USER));
        Assert.assertNotNull(repository.add(ADMIN_TEST));
        @Nullable final User user = repository.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST, user);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(repository.add(USER_LIST_ADDED));
        for (final User user : USER_LIST_ADDED)
            Assert.assertEquals(user, repository.findOneById(user.getId()));
    }

    @Test
    public void set() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        emptyRepository.set(USER_LIST);
        Assert.assertEquals(USER_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        Assert.assertEquals(USER_LIST_ADDED, emptyRepository.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void findOneByIndex() {
        Assert.assertNull(repository.findOneByIndex(null));
        final int index = repository.findAll().indexOf(USER_TEST);
        @Nullable final User user = repository.findOneByIndex(index);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void clear() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST_ADDED);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void remove() {
        Assert.assertNull(repository.remove(null));
        @Nullable final User createdUser = repository.add(ADMIN_TEST);
        @Nullable final User removedUser = repository.remove(createdUser);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_USER_ID));
        @Nullable final User createdUser = repository.add(ADMIN_TEST);
        @Nullable final User removedUser = repository.removeById(ADMIN_TEST.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final User createdUser = repository.add(ADMIN_TEST);
        final int index = repository.findAll().indexOf(createdUser);
        @Nullable final User removedUser = repository.removeByIndex(index);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_TEST);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST);
        emptyRepository.removeAll(USER_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void create() {
        @NotNull final User user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD));
        Assert.assertEquals(user, repository.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() {
        @NotNull final User user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), ADMIN_TEST_EMAIL);
        Assert.assertEquals(user, repository.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void createWithRole() {
        @NotNull final User user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), Role.ADMIN);
        Assert.assertEquals(user, repository.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() {
        Assert.assertNull(repository.findByLogin(null));
        @Nullable final User user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void findByEmail() {
        Assert.assertNull(repository.findByEmail(null));
        @Nullable final User user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(repository.isLoginExists(null));
        Assert.assertTrue(repository.isLoginExists(USER_TEST.getLogin()));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(repository.isEmailExists(null));
        Assert.assertTrue(repository.isEmailExists(USER_TEST.getEmail()));
    }

}
