package ru.t1.simanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.repository.IProjectRepository;
import ru.t1.simanov.tm.api.repository.ITaskRepository;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.api.service.IUserService;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.exception.entity.UserNotFoundException;
import ru.t1.simanov.tm.exception.field.*;
import ru.t1.simanov.tm.exception.user.AccessDeniedException;
import ru.t1.simanov.tm.exception.user.ExistsLoginException;
import ru.t1.simanov.tm.exception.user.RoleEmptyException;
import ru.t1.simanov.tm.marker.UnitCategory;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.repository.ProjectRepository;
import ru.t1.simanov.tm.repository.TaskRepository;
import ru.t1.simanov.tm.repository.UserRepository;

import java.util.Collections;

import static ru.t1.simanov.tm.constant.ProjectTestData.ADMIN_PROJECT1;
import static ru.t1.simanov.tm.constant.TaskTestData.ADMIN_TASK1;
import static ru.t1.simanov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserService service = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Before
    public void before() {
        userRepository.add(USER_TEST);
    }

    @After
    public void after() {
        userRepository.removeAll(USER_LIST);
    }

    @Test
    public void add() {
        Assert.assertNull(service.add(NULL_USER));
        Assert.assertNotNull(service.add(ADMIN_TEST));
        @Nullable final User user = service.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST, user);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(USER_LIST_ADDED));
        for (final User user : USER_LIST_ADDED)
            Assert.assertEquals(user, service.findOneById(user.getId()));
    }

    @Test
    public void set() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_LIST_ADDED);
        emptyService.set(USER_LIST);
        Assert.assertEquals(USER_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_LIST_ADDED);
        Assert.assertEquals(USER_LIST_ADDED, emptyService.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = service.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void findOneByIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(service.getSize());
        });
        final int index = service.findAll().indexOf(USER_TEST);
        @Nullable final User user = service.findOneByIndex(index);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void clear() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_LIST_ADDED);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_USER_ID));
        @Nullable final User createdUser = service.add(ADMIN_TEST);
        @Nullable final User removedUser = service.removeById(ADMIN_TEST.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(service.getSize());
        });
        @Nullable final User createdUser = service.add(ADMIN_TEST);
        final int index = service.findAll().indexOf(createdUser);
        @Nullable final User removedUser = service.removeByIndex(index);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_TEST);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void removeAll() {
        @NotNull final IUserRepository userEmptyRepository = new UserRepository();
        @NotNull final IPropertyService propertyEmptyService = new PropertyService();
        @NotNull final IProjectRepository projectEmptyRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskEmptyRepository = new TaskRepository();
        @NotNull final IUserService emptyService = new UserService(propertyEmptyService, userEmptyRepository, projectEmptyRepository, taskEmptyRepository);
        Assert.assertThrows(AccessDeniedException.class, () -> {
            emptyService.removeAll(null);
        });
        Assert.assertThrows(AccessDeniedException.class, () -> {
            emptyService.removeAll(Collections.emptyList());
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_LIST_ADDED);
        emptyService.removeAll(USER_LIST_ADDED);
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void create() {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "");
        });
        @NotNull final User user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithRole() {
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "", role);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @NotNull final Role nullRole = null;
            service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, nullRole);
        });
        @NotNull final User user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, Role.ADMIN);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin("");
        });
        @Nullable final User user = service.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void findByEmail() {
        Assert.assertNull(service.findByEmail(null));
        Assert.assertNull(service.findByEmail(""));
        @Nullable final User user = service.findByEmail(USER_TEST_EMAIL);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST, user);
    }

    @Test
    public void remove() {
        Assert.assertNull(service.remove(null));
        @Nullable final User createdUser = service.add(ADMIN_TEST);
        projectRepository.add(ADMIN_PROJECT1);
        taskRepository.add(ADMIN_TASK1);
        @Nullable final User removedUser = service.remove(createdUser);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST, removedUser);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
        Assert.assertNull(projectRepository.findOneById(ADMIN_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.removeByLogin(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_TEST);
        service.removeByLogin(ADMIN_TEST_LOGIN);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void removeByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.removeByEmail(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_TEST);
        service.removeByEmail(ADMIN_TEST_EMAIL);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USER_TEST.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USER_TEST.getId(), "");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.setPassword(NON_EXISTING_USER_ID, ADMIN_TEST_PASSWORD);
        });
        service.setPassword(USER_TEST.getId(), ADMIN_TEST_PASSWORD);
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), USER_TEST.getPasswordHash());
        service.setPassword(USER_TEST.getId(), USER_TEST_PASSWORD);
    }

    @Test
    public void updateUser() {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser(null, firstName, lastName, middleName);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser("", firstName, lastName, middleName);
        });
        service.updateUser(USER_TEST.getId(), firstName, lastName, middleName);
        Assert.assertEquals(firstName, USER_TEST.getFirstName());
        Assert.assertEquals(lastName, USER_TEST.getLastName());
        Assert.assertEquals(middleName, USER_TEST.getMiddleName());
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(service.isLoginExists(null));
        Assert.assertFalse(service.isLoginExists(""));
        Assert.assertTrue(service.isLoginExists(USER_TEST_LOGIN));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(service.isEmailExists(null));
        Assert.assertFalse(service.isEmailExists(""));
        Assert.assertTrue(service.isEmailExists(USER_TEST_EMAIL));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.lockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_TEST);
        service.lockUserByLogin(ADMIN_TEST_LOGIN);
        Assert.assertTrue(ADMIN_TEST.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.unlockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_TEST);
        service.lockUserByLogin(ADMIN_TEST_LOGIN);
        service.unlockUserByLogin(ADMIN_TEST_LOGIN);
        Assert.assertFalse(ADMIN_TEST.getLocked());
    }

}
