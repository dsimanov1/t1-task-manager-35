package ru.t1.simanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.repository.ISessionRepository;
import ru.t1.simanov.tm.api.service.ISessionService;
import ru.t1.simanov.tm.exception.field.IdEmptyException;
import ru.t1.simanov.tm.exception.field.IndexIncorrectException;
import ru.t1.simanov.tm.exception.field.UserIdEmptyException;
import ru.t1.simanov.tm.exception.user.AccessDeniedException;
import ru.t1.simanov.tm.marker.UnitCategory;
import ru.t1.simanov.tm.model.Session;
import ru.t1.simanov.tm.repository.SessionRepository;

import java.util.Collections;

import static ru.t1.simanov.tm.constant.SessionTestData.*;
import static ru.t1.simanov.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @NotNull
    private final ISessionService service = new SessionService(repository);

    @Before
    public void before() {
        repository.add(USER_SESSION1);
        repository.add(USER_SESSION2);
    }

    @After
    public void after() {
        repository.removeAll(SESSION_LIST);
    }

    @Test
    public void add() {
        Assert.assertNull(service.add(NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_SESSION1));
        @Nullable final Session session = service.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addByUserId() {
        Assert.assertNull(service.add(ADMIN_TEST.getId(), NULL_SESSION));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_SESSION1);
        });
        Assert.assertNotNull(service.add(ADMIN_TEST.getId(), ADMIN_SESSION1));
        @Nullable final Session session = service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void addMany() {
        Assert.assertNotNull(service.add(ADMIN_SESSION_LIST));
        for (final Session session : ADMIN_SESSION_LIST)
            Assert.assertEquals(session, service.findOneById(session.getId()));
    }

    @Test
    public void set() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionService emptyService = new SessionService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION_LIST);
        emptyService.set(ADMIN_SESSION_LIST);
        Assert.assertEquals(ADMIN_SESSION_LIST, emptyService.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionService emptyService = new SessionService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION_LIST);
        Assert.assertEquals(USER_SESSION_LIST, emptyService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        Assert.assertEquals(USER_SESSION_LIST, service.findAll(USER_TEST.getId()));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertFalse(service.existsById(USER_TEST.getId(), null));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), ""));
        Assert.assertFalse(service.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId(), USER_SESSION1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_TEST.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_SESSION1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_SESSION1.getId());
        });
        Assert.assertNull(service.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USER_TEST.getId(), USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(service.getSize());
        });
        final int index = service.findAll().indexOf(USER_SESSION1);
        @Nullable final Session session = service.findOneByIndex(index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void findOneByIndexByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), -1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USER_TEST.getId(), service.getSize());
        });
        final int index = service.findAll().indexOf(USER_SESSION1);
        @Nullable final Session session = service.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1, session);
    }

    @Test
    public void clear() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionService emptyService = new SessionService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION_LIST);
        emptyService.clear();
        Assert.assertEquals(0, emptyService.getSize());
    }

    @Test
    public void clearByUserId() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionService emptyService = new SessionService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.clear("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(USER_SESSION1);
        emptyService.add(USER_SESSION2);
        emptyService.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyService.getSize(USER_TEST.getId()));
    }

    @Test
    public void remove() {
        Assert.assertNull(service.remove(null));
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.remove(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertNull(service.remove(ADMIN_TEST.getId(), null));
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.remove(ADMIN_TEST.getId(), createdSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_SESSION_ID));
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.removeById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_TEST.getId(), "");
        });
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(service.removeById(ADMIN_TEST.getId(), USER_SESSION1.getId()));
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        @Nullable final Session removedSession = service.removeById(ADMIN_TEST.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId(), ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(service.getSize());
        });
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        final int index = service.findAll().indexOf(createdSession);
        @Nullable final Session removedSession = service.removeByIndex(index);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN_SESSION1, removedSession);
        Assert.assertNull(service.findOneById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionService emptyService = new SessionService(emptyRepository);
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize());
        emptyService.add(ADMIN_SESSION1);
        Assert.assertEquals(1, emptyService.getSize());
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionService emptyService = new SessionService(emptyRepository);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            emptyService.getSize("");
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        Assert.assertEquals(0, emptyService.getSize(ADMIN_TEST.getId()));
        emptyService.add(ADMIN_SESSION1);
        emptyService.add(USER_SESSION1);
        Assert.assertEquals(1, emptyService.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() {
        @NotNull final ISessionRepository emptyRepository = new SessionRepository();
        @NotNull final ISessionService emptyService = new SessionService(emptyRepository);
        Assert.assertThrows(AccessDeniedException.class, () -> {
            emptyService.removeAll(null);
        });
        Assert.assertThrows(AccessDeniedException.class, () -> {
            emptyService.removeAll(Collections.emptyList());
        });
        Assert.assertTrue(emptyService.findAll().isEmpty());
        emptyService.add(SESSION_LIST);
        emptyService.removeAll(SESSION_LIST);
        Assert.assertEquals(0, emptyService.getSize());
    }

}
